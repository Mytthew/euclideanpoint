package com.company;

import java.awt.*;

public final class EuclideanPoint {
	private final double x;
	private final double y;
	private final int weight;
	private final String label;
	private final java.awt.Color color;

	public EuclideanPoint(double x, double y, int weight, String label, Color color) {
		this.x = x;
		this.y = y;
		this.weight = weight;
		this.label = label;
		this.color = color;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getWeight() {
		return weight;
	}

	public String getLabel() {
		return label;
	}

	public Color getColor() {
		return color;
	}
}
